package fendri.inovatif.co.id.googlemapsapi.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Asus on 09/04/2018.
 */

public class InitLibrary {
    public static String URL_API = "http://192.168.43.50:8888/maps/api/";
    public static String URL_FOTO = "http://192.168.43.50:8888/maps/upload/";

    public static Retrofit setInit(){
        return new Retrofit.Builder().baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiServices getInstance(){
        return setInit().create(ApiServices.class);
    }
}
