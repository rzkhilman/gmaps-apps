package fendri.inovatif.co.id.googlemapsapi.network;

import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseAdd;
import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseCategory;
import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseDelete;
import fendri.inovatif.co.id.googlemapsapi.network.response.detail.ResponseDetailPlace;
import fendri.inovatif.co.id.googlemapsapi.network.response.places.ResponsePlaces;
import fendri.inovatif.co.id.googlemapsapi.network.response.search.ResponseSearch;
import fendri.inovatif.co.id.googlemapsapi.network.response.searchdetail.ResponseSearchDetail;
import fendri.inovatif.co.id.googlemapsapi.network.response.type_place.ResponseType;
import fendri.inovatif.co.id.googlemapsapi.network.response.update.ResponseUpdate;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Asus on 09/04/2018.
 */

public interface ApiServices {
    // Definisikan semua reuqest
    // Method, End Kemana, Paremeter yg dibawa, model penampung Response

    @GET("get-category.php")
    Call<ResponseCategory> request_category();

    @GET("get-places.php")
    Call<ResponsePlaces> request_places(
            @Query("category_id") String category_id
    );

    //http://localhost/maps/api/get-search-detail.php?place_id=1

    @GET("get-search-detail.php")
    Call<ResponseSearchDetail> request_searchdetail(
            @Query("place_id") String place_id
    );

    //http://localhost/maps/api/detail-place.php?latitude=-2.9511763&longitude=104.7783416
    @GET("detail-place.php")
    Call<ResponseDetailPlace> request_detail(
            @Query("latitude") Double latitude,
            @Query("longitude") Double longitude
    );

    //http://localhost/maps/api/search-places.php?keyword=a
    @GET("search-places.php")
    Call<ResponseSearch> request_search(
            @Query("keyword") String keyword
    );

    @POST("add-place.php")
    @FormUrlEncoded // kalau post harus pakai ini
    Call<ResponseAdd> request_insert(
            @Field("category_id") String category_id,
            @Field("name") String name,
            @Field("latitude") String latitude,
            @Field("longitude") String longitude,
            @Field("street_address") String street_address,
            @Field("type") String type,
            @Field("phone") String phone,
            @Field("photo") String photo
    );


    //https://github.com/jrvansuita/PickImage
    //untuk upload data yang ada fotonya
    @Multipart
    @POST("add-place.php")
    Call<ResponseAdd> request_insert_place (
            @Part MultipartBody.Part file, // Parameter ini untuk membawa file
            @Part("category_id") RequestBody category_id, // Parameter ini bisa membawa semua Tipe data, tdk hanya String
            @Part("name") RequestBody name,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("street_address") RequestBody street_address,
            @Part("type") RequestBody type,
            @Part("phone") RequestBody phone
    );

    @GET("get-type.php")
    Call<ResponseType> request_type();

    // TODO : tambahan
    @GET("delete-place.php")
    Call<ResponseDelete> request_delete(
            @Query("place_id") String place_id
    );

    //untuk upload data yang ada fotonya
    @Multipart
    @POST("edit-place.php")
    Call<ResponseUpdate> request_update_place (
            @Part MultipartBody.Part file, // Parameter ini untuk membawa file
            @Part("place_id") RequestBody place_id,
            @Part("category_id") RequestBody category_id, // Parameter ini bisa membawa semua Tipe data, tdk hanya String
            @Part("name") RequestBody name,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("street_address") RequestBody street_address,
            @Part("type") RequestBody type,
            @Part("phone") RequestBody phone
    );

}
