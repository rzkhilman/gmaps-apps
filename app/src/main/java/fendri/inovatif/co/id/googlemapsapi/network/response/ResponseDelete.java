package fendri.inovatif.co.id.googlemapsapi.network.response;

import com.google.gson.annotations.SerializedName;

public class ResponseDelete{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private Object data;

	@SerializedName("isSuccess")
	private boolean isSuccess;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(Object data){
		this.data = data;
	}

	public Object getData(){
		return data;
	}

	public void setIsSuccess(boolean isSuccess){
		this.isSuccess = isSuccess;
	}

	public boolean isIsSuccess(){
		return isSuccess;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDelete{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",isSuccess = '" + isSuccess + '\'' + 
			"}";
		}
}