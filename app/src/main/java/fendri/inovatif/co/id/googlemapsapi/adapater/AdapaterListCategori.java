package fendri.inovatif.co.id.googlemapsapi.adapater;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fendri.inovatif.co.id.googlemapsapi.R;
import fendri.inovatif.co.id.googlemapsapi.activity.PlacesActivity;
import fendri.inovatif.co.id.googlemapsapi.network.InitLibrary;
import fendri.inovatif.co.id.googlemapsapi.network.response.DataItem;

/**
 * Created by Asus on 08/04/2018.
 */

//penambahan extends RecyclerView.Adapter<AdapaterListCategori.CategoryViewHolder> {

public class AdapaterListCategori extends RecyclerView.Adapter<AdapaterListCategori.CategoryViewHolder> {

    Context context;
    List<DataItem> data_category;
    public AdapaterListCategori(Context context, List<DataItem> data_category) {
        this.context = context;
        this.data_category = data_category;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View catView = LayoutInflater.from(context).inflate(R.layout.category_item, parent, false);

        CategoryViewHolder holder  = new CategoryViewHolder(catView);

        return holder;
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final String cat_name = data_category.get(position).getTitle();
        String cat_ket = data_category.get(position).getKet();
        String cat_gambar = data_category.get(position).getGambar();
        final String cat_id = data_category.get(position).getId();
        holder.tvCatName.setText(cat_name);
        holder.tvCatKet.setText(cat_ket);
        Picasso.with(context).load(InitLibrary.URL_FOTO+""+cat_gambar).into(holder.ivgambar);

        //event click pada list data
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //go to MapsActivity.class
                Intent intent = new Intent(context, PlacesActivity.class);
                intent.putExtra("CAT_ID", cat_id);
                intent.putExtra("CAT_NAME", cat_name);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        //jumlah list data yang akan ditampilkan
        return data_category.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvCatName, tvCatKet;
        ImageView ivgambar;
        public CategoryViewHolder(View itemView) {
            super(itemView);
            tvCatName = itemView.findViewById(R.id.tvCatName);
            tvCatKet = itemView.findViewById(R.id.tvCatKet);
            ivgambar = itemView.findViewById(R.id.ivgambar);
        }
    }
}
