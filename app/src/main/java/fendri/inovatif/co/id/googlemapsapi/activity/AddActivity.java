package fendri.inovatif.co.id.googlemapsapi.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fendri.inovatif.co.id.googlemapsapi.MainActivity;
import fendri.inovatif.co.id.googlemapsapi.R;
import fendri.inovatif.co.id.googlemapsapi.network.ApiServices;
import fendri.inovatif.co.id.googlemapsapi.network.InitLibrary;
import fendri.inovatif.co.id.googlemapsapi.network.response.DataItem;
import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseAdd;
import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseCategory;
import fendri.inovatif.co.id.googlemapsapi.network.response.type_place.ResponseType;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity implements IPickResult {

    @BindView(R.id.etPlaceName)
    EditText etPlaceName;
    @BindView(R.id.spPlaceCategory)
    Spinner spPlaceCategory;
    @BindView(R.id.etLatitude)
    EditText etLatitude;
    @BindView(R.id.etLongitude)
    EditText etLongitude;
    @BindView(R.id.btnOpenMap)
    ImageButton btnOpenMap;
    @BindView(R.id.etStreetAddress)
    EditText etStreetAddress;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.btnSelectPhoto)
    Button btnSelectPhoto;
    @BindView(R.id.tvFilename)
    TextView tvFilename;
    @BindView(R.id.btnSavePlace)
    Button btnSavePlace;
    @BindView(R.id.spPlaceType)
    Spinner spPlaceType;
    private double data_latitude, data_longitude;
    List<DataItem> data_category;
    List<fendri.inovatif.co.id.googlemapsapi.network.response.type_place.DataItem> data_type_place;
    String selected_category_id = "0";
    String selected_type_place = "none";

    private Uri uri;
    private String filePath;
    private File file;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);
        //
        initComponent();
        // listener ketika SPinnser di ubah
        spPlaceCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // set ke variable
                selected_category_id = data_category.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPlaceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_type_place = data_type_place.get(position).getType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initComponent() {
        // Ambil data Category ke API untuk di isi ke Spinner
        ApiServices api = InitLibrary.getInstance();
        Call<ResponseCategory> call = api.request_category();
        call.enqueue(new Callback<ResponseCategory>() {
            @Override
            public void onResponse(Call<ResponseCategory> call, Response<ResponseCategory> response) {
                // pastikan response normal
                if (response.isSuccessful()) {
                    // tampung data dalam array
                    boolean isSuccess = response.body().isIsSuccess();
                    data_category = response.body().getData(); //buat menjadi globalvariable

                    if (isSuccess) {
                        // siapkan Adapter untuk spinnser, kita pakai ArrayAdapter saja
                        ArrayList<String> categories = new ArrayList<>();
                        // foreach Object List Category
                        for (DataItem cat : data_category) {
                            // masukan Title kedalam Object ArrayList
                            categories.add(cat.getTitle());
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddActivity.this, android.R.layout.simple_spinner_dropdown_item, categories);
                        spPlaceCategory.setAdapter(adapter);
                    }
                } else {
                    Toast.makeText(AddActivity.this, "Terjadi kesalahan " + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseCategory> call, Throwable t) {
                t.printStackTrace();
            }
        });

        // Buat Call baru untuk mendapatkan tyoe place
        Call<ResponseType> call1TypePlace = api.request_type();
        call1TypePlace.enqueue(new Callback<ResponseType>() {
            @Override
            public void onResponse(Call<ResponseType> call, Response<ResponseType> response) {
                // pastikan response normal
                if (response.isSuccessful()) {
                    // tampung data dalam array
                    boolean isSuccess = response.body().isIsSuccess();
                    data_type_place = response.body().getData(); //buat menjadi globalvariable

                    if (isSuccess) {
                        // siapkan Adapter untuk spinnser, kita pakai ArrayAdapter saja
                        ArrayList<String> categories = new ArrayList<>();
                        // foreach Object List Category
                        for (fendri.inovatif.co.id.googlemapsapi.network.response.type_place.DataItem type : data_type_place) {
                            // masukan Title kedalam Object ArrayList
                            categories.add(type.getType());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddActivity.this, android.R.layout.simple_spinner_dropdown_item, categories);
                        spPlaceType.setAdapter(adapter);
                    }
                } else {
                    Toast.makeText(AddActivity.this, "Terjadi kesalahan " + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseType> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
    // Kita gunakan OnActivity Resul tuntuk mendpatkan data dari Intent

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data); // spertinya tadi sya hapus
        if (requestCode == 22) { // 22 yg tadi kita tentukan
            if (resultCode == RESULT_OK) {
                data_latitude = data.getDoubleExtra("LAT", 0);
                data_longitude = data.getDoubleExtra("LONG", 0);
                // langung kita set ke EditText
                etLatitude.setText(String.valueOf(data_latitude));// convert double ke String
                etLongitude.setText(String.valueOf(data_longitude));// convert double ke String
                etStreetAddress.requestFocus();
            }
        }
    }

    @OnClick(R.id.btnOpenMap)
    public void onBtnOpenMapClicked() {
        //
        Intent intent = new Intent(this, SelectMapActivity.class);
        startActivityForResult(intent, 22); // 22 itu request code aja, bebas kita yg menentukan
    }

    @OnClick(R.id.btnSelectPhoto)
    public void onBtnSelectPhotoClicked() {
        PickImageDialog.build(new PickSetup()).show(this);
    }

    @OnClick(R.id.btnSavePlace)
    public void onBtnSavePlaceClicked() {

        //input data tidak menggunakan upload foto
       /* String category_id = selected_category_id; // dummy dulu
        String name = etPlaceName.getText().toString();
        String latitude = etLatitude.getText().toString();
        String longitude = etLongitude.getText().toString();
        String street_address = etStreetAddress.getText().toString();
        String phone = etPhoneNumber.getText().toString();
        String type = selected_type_place;
        String photo = "image.png";*/

        //https://github.com/jrvansuita/PickImage
       //untuk input data menggunakan upload foto
        //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("photo", file.getName(), mFile);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody street_address = RequestBody.create(MediaType.parse("text/plain"), etStreetAddress.getText().toString());
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), etPlaceName.getText().toString());
        RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"), etLatitude.getText().toString());
        RequestBody longitude = RequestBody.create(MediaType.parse("text/plain"), etLongitude.getText().toString());
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), etPhoneNumber.getText().toString());
        RequestBody type = RequestBody.create(MediaType.parse("text/plain"), selected_type_place);
        RequestBody category_id = RequestBody.create(MediaType.parse("text/plain"), selected_category_id);
        // Instance Retrofit
        ApiServices api = InitLibrary.getInstance();
        // Siapkan Request
        Call<ResponseAdd> call = api.request_insert_place(fileToUpload,
                category_id,
                name,
                latitude,
                longitude,
                street_address,
                type,
                phone);
        // Kirim
        call.enqueue(new Callback<ResponseAdd>() {
            @Override
            public void onResponse(Call<ResponseAdd> call, Response<ResponseAdd> response) {
                if (response.isSuccessful()) {
                    boolean isSuccess = response.body().isIsSuccess();
                    String msg = response.body().getMsg();
                    if (isSuccess) {
                        // Pindah ke activity utama
                        startActivity(new Intent(AddActivity.this, MainActivity.class));
                    }
                    // munculkan message
                    Toast.makeText(AddActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseAdd> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            //If you want the Bitmap.
            //getImageView().setImageBitmap(r.getBitmap());

            //Image path
            //r.getPath();
            // Simpan Gambar ke global variable
            //https://github.com/jrvansuita/PickImage
            uri = r.getUri();
            Log.d("log", uri.toString());
            filePath = getRealPathFromURIPath(uri, AddActivity.this);
            file = new File(filePath);
            // Set name ke widge
            tvFilename.setText(file.getName());

        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }

    }
    //https://github.com/jrvansuita/PickImage
    //untuk upload foto
    private String getRealPathFromURIPath(Uri contentURI, AddActivity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
}
