package fendri.inovatif.co.id.googlemapsapi;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fendri.inovatif.co.id.googlemapsapi.activity.AddActivity;
import fendri.inovatif.co.id.googlemapsapi.activity.SearcActivity;
import fendri.inovatif.co.id.googlemapsapi.adapater.AdapaterListCategori;
import fendri.inovatif.co.id.googlemapsapi.network.ApiServices;
import fendri.inovatif.co.id.googlemapsapi.network.InitLibrary;
import fendri.inovatif.co.id.googlemapsapi.network.response.DataItem;
import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseCategory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CALL_SMS_LOCATION = 1;
    @BindView(R.id.rvCategoryList)
    RecyclerView rvCategoryList;
    @BindView(R.id.fabTambah)
    FloatingActionButton fabTambah;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        checkPermission();
        // Layout manager
        rvCategoryList.setLayoutManager(new LinearLayoutManager(this));
        //buat getCategory
        getCategory();

    }

    private void checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.CALL_PHONE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_CALL_SMS_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_SMS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void getCategory() {
        ApiServices api = InitLibrary.getInstance();
        Call<ResponseCategory> call = api.request_category();
        call.enqueue(new Callback<ResponseCategory>() {
            @Override
            public void onResponse(Call<ResponseCategory> call, Response<ResponseCategory> response) {
                if (response.isSuccessful()) {
                    Log.d("data didownload", response.body().toString());
                    boolean isSuccess = response.body().isIsSuccess();
                    List<DataItem> data_category = response.body().getData();
                    if (isSuccess) { // jika data ada
                        // panggil adapter
                        AdapaterListCategori adapter = new AdapaterListCategori(MainActivity.this, data_category);
                        rvCategoryList.setAdapter(adapter);
                    } else {
                        Toast.makeText(MainActivity.this, "Data kategori kosong", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseCategory> call, Throwable t) {

            }
        });
    }

    // METHOD UNTUK MEMUNCULKAN MENU DI ACTION BAR

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_meu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        switch (item_id) {
            case R.id.nav_search: // kalau icon yang di klik idnya nav_search
                // pindah activity search
                Intent mIntent = new Intent(this, SearcActivity.class);
                startActivity(mIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fabTambah)
    public void onFabClicked() {
        startActivity(new Intent(this, AddActivity.class));
    }
}
