package fendri.inovatif.co.id.googlemapsapi.adapater;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fendri.inovatif.co.id.googlemapsapi.R;
import fendri.inovatif.co.id.googlemapsapi.activity.PlacesActivity;
import fendri.inovatif.co.id.googlemapsapi.activity.SearchDetailActivity;
import fendri.inovatif.co.id.googlemapsapi.network.InitLibrary;
import fendri.inovatif.co.id.googlemapsapi.network.response.search.DataItem;

/**
 * Created by Asus on 08/04/2018.
 */

//penambahan extends RecyclerView.Adapter<AdapaterListCategori.CategoryViewHolder> {

public class AdapaterSearchList extends RecyclerView.Adapter<AdapaterSearchList.PlaceViewHolder> {

    Context context;
    List<DataItem> data_place;
    public AdapaterSearchList(Context context, List<DataItem> data_place) {
        this.context = context;
        this.data_place = data_place;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View searchView = LayoutInflater.from(context).inflate(R.layout.search_item, parent, false);

        PlaceViewHolder holder  = new PlaceViewHolder(searchView);

        return holder;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
       final String place_name = data_place.get(position).getName();
        final String place_title = data_place.get(position).getTitle();
       String place_photo = data_place.get(position).getPhoto();
       final String place_id = data_place.get(position).getId();
       holder.tvPlaceName.setText(place_name);
       holder.tvPlaceTitle.setText(place_title);
       Picasso.with(context).load(InitLibrary.URL_FOTO+""+place_photo).into(holder.ivPlacePhoto);
        //event click pada list data
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View v) {
               //go to MapsActivity.class
               Intent intent = new Intent(context, SearchDetailActivity.class);
               intent.putExtra("PLACE_ID", place_id);
               intent.putExtra("PLACE_NAME", place_name);
                context.startActivity(intent);
           }
        });

    }

    @Override
    public int getItemCount() {
        //jumlah list data yang akan ditampilkan
        return data_place.size();
    }

public class PlaceViewHolder extends RecyclerView.ViewHolder {
    TextView tvPlaceName, tvPlaceTitle;
    ImageView ivPlacePhoto;
    public PlaceViewHolder(View itemView) {
        super(itemView);
        tvPlaceName = itemView.findViewById(R.id.tvPlaceName);
        tvPlaceTitle = itemView.findViewById(R.id.tvPlaceTitle);
        ivPlacePhoto = itemView.findViewById(R.id.ivPlacePhoto);
    }
}
}
