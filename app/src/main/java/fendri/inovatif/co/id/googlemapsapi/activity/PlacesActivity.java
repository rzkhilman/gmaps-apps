package fendri.inovatif.co.id.googlemapsapi.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import fendri.inovatif.co.id.googlemapsapi.R;
import fendri.inovatif.co.id.googlemapsapi.network.ApiServices;
import fendri.inovatif.co.id.googlemapsapi.network.InitLibrary;
import fendri.inovatif.co.id.googlemapsapi.network.response.ResponseDelete;
import fendri.inovatif.co.id.googlemapsapi.network.response.detail.Data;
import fendri.inovatif.co.id.googlemapsapi.network.response.detail.ResponseDetailPlace;
import fendri.inovatif.co.id.googlemapsapi.network.response.places.DataItem;
import fendri.inovatif.co.id.googlemapsapi.network.response.places.ResponsePlaces;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlacesActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{

    private GoogleMap mMap;
    String cat_id;
    String cat_name, pilih_name;

    // Deklarasi
    LinearLayout layoutBottomSheet;
    BottomSheetBehavior sheetBehavior;

    TextView tvPlaceName, tvLatLong, tvStreetAddress, tvPhone, actCall, actSms, actWa;
    Button actEdit, actDel;
    ImageView ivGambarTempat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initWidget();
        bottomSheetInit();
        cat_id =  getIntent().getStringExtra("CAT_ID");
        cat_name =  getIntent().getStringExtra("CAT_NAME");

        //menampilkan cat_name di actionbar
        getSupportActionBar().setTitle(cat_name);
        Toast.makeText(this, "" + cat_id, Toast.LENGTH_SHORT).show();

    }

    private void initWidget() {
        tvLatLong = findViewById(R.id.tvLatLong);
        tvPlaceName = findViewById(R.id.tvPlaceName);
        tvStreetAddress = findViewById(R.id.tvStreetAddress);
        tvPhone = findViewById(R.id.tvPhone);
        actCall = findViewById(R.id.actCall);
        actSms = findViewById(R.id.actSms);
        actWa = findViewById(R.id.actWa);
        actEdit = findViewById(R.id.actEdit);
        actDel = findViewById(R.id.actDel);
        ivGambarTempat = findViewById(R.id.ivGambarTempat);

    }

    //membuat tampilan keterangan hide atau show
    private void bottomSheetInit() {
        // Inisialisasi
        layoutBottomSheet = findViewById(R.id.bottomSheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    public void initComponent(){
        ApiServices api = InitLibrary.getInstance();
        retrofit2.Call<ResponsePlaces> call = api.request_places(cat_id);
        call.enqueue(new retrofit2.Callback<ResponsePlaces>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsePlaces> call, retrofit2.Response<ResponsePlaces> response) {

                if (response.isSuccessful()){
                    Log.d("Respon body ", response.body().toString());
                    boolean isSuccess = response.body().isIsSuccess();
                    // kita hanya akan membuat marker ketika data place nya ada
                    if (isSuccess){
                        List<DataItem> data_places = response.body().getData();

                        // zoom di tengah2 semua marker
                        LatLngBounds.Builder latLongBuilder = new LatLngBounds.Builder();

                        // kita buat looping untuk membuat marker
                        for(DataItem place : data_places){
                            // kita pisahkan datanya
                            // LatLng parameternya harus bernilai Double
                            LatLng place_latlong = new LatLng(Double.parseDouble(place.getLatitude()), Double.parseDouble(place.getLongitude()));
                            String place_name = place.getName();
                            String place_phone = place.getPhone();
                            String place_steet_address = place.getStreetAddress();
                            String place_type = place.getType();
                            String place_id = place.getId();

                            latLongBuilder.include(place_latlong);
                            // ini bakal jadi default markernya, mungkin tambahin 1 icond efault
                            int src_icon = R.drawable.marker_default;
                            switch (place_type){
                                case "hotel":
                                    src_icon = R.drawable.marker_hotel; // tidak bisa, kalau pun dibuat teteap pakai switcth / if
                                    /* jadi sama saja, karena iconya kan ngarahin ke drawable */
                                    break;
                                case "restaurant":
                                    src_icon = R.drawable.marker_restaurant;
                                    break;
                                case "mall":
                                    src_icon = R.drawable.marker_mall;
                                    break;


                            }
                            mMap.addMarker(new MarkerOptions().position(place_latlong).title(place_name)
                                    .icon(BitmapDescriptorFactory.fromResource(src_icon)));
                        }

                        //dapatkan koordinat di tengah-tengah
                        LatLngBounds bounds = latLongBuilder.build();
                        // Padding menyesuaikan lebar dan tinggi ukuran layar device
                        int width = getResources().getDisplayMetrics().widthPixels;
                        int height = getResources().getDisplayMetrics().heightPixels;
                        int paddingMap = (int)(width * 0.1);//jarak dari sisi map 30%
                        // atur zoom ke koordinat
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, paddingMap);
                        mMap.animateCamera(cu); // animasi zoom

                    }
                } else {
                    Toast.makeText(PlacesActivity.this, "Tidak ada tempat dalam kategori ini", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<ResponsePlaces> call, Throwable t) {
                // spertinya error
                // coba kita trace
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // hide bottom sheet
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        // buat initComponent
        initComponent();
    }
    public void showHideBottomSheet() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        //Toast.makeText(this, "Marker di klik", Toast.LENGTH_SHORT).show();
        showHideBottomSheet();
        tvPlaceName.setText(marker.getTitle());
        tvLatLong.setText("LatLong : " + marker.getPosition().latitude + "," + marker.getPosition().longitude);
        // siapkan request
        ApiServices api = InitLibrary.getInstance();
        retrofit2.Call<ResponseDetailPlace> call = api.request_detail(marker.getPosition().latitude, marker.getPosition().longitude);
        // kirim request
        call.enqueue(new retrofit2.Callback<ResponseDetailPlace>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(retrofit2.Call<ResponseDetailPlace> call, retrofit2.Response<ResponseDetailPlace> response) {
                if (response.isSuccessful()){
                    boolean isSuccess = response.body().isIsSuccess();
                    final Data data = response.body().getData();
                    String msg = response.body().getMsg();
                    // jika isSuccess = true = data ada. isSuccess = false = data tidak ada
                    if (isSuccess){
                        // tinggal kita set Aja ke widget
                        tvStreetAddress.setText(data.getStreetAddress());
                        tvPhone.setText("Phone : " + data.getPhone());
                        // Foto
                        Picasso.with(PlacesActivity.this)
                                .load(InitLibrary.URL_FOTO + "" + data.getPhoto()).into(ivGambarTempat);
                        // Call phone
                        final String phone = data.getPhone();

                        actCall.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String uri = "tel:" + phone.trim() ;
                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse(uri));
                                startActivity(intent);
                            }
                        });
                        actSms.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = Uri.parse("smsto:" + phone);
                                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                                it.putExtra("sms_body", "Isi Smsnya disini");
                                startActivity(it);                             }
                        });

                        actWa.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PackageManager pm=getPackageManager();
                                try {

                                    PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                                    Uri uri = Uri.parse("smsto:");
                                    Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                                    i.putExtra("sms_body", "3asd");
                                    i.setPackage("com.whatsapp");
                                    startActivity(i);

                                } catch (PackageManager.NameNotFoundException e) {
                                    Toast.makeText(PlacesActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            }
                        });
                        // TODO : Tambahan Untuk Edit dan Delete
                        actDel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // TODO : Alert dialog hapus 
                                confirmDelete(data.getId());
                            }
                        });
                        // TODO : Edit
                        actEdit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // intent
                                Intent intent = new Intent(PlacesActivity.this, EditActivity.class);
                                intent.putExtra("PLACE_ID", data.getId());
                                intent.putExtra("PLACE_NAME", data.getName());
                                intent.putExtra("PLACE_LAT", data.getLatitude());
                                intent.putExtra("PLACE_LONG", data.getLongitude());
                                intent.putExtra("PLACE_PHONE", data.getPhone());
                                intent.putExtra("PLACE_ADDR", data.getStreetAddress());
                                intent.putExtra("PLACE_TYPE", data.getType());
                                intent.putExtra("PLACE_CAT_ID", data.getCategoryId());
                                // Mulai activity edit
                                startActivity(intent);
                            }
                        });
                    } else {
                        // tampilkan Toast
                        Toast.makeText(PlacesActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.d("Gagal", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseDetailPlace> call, Throwable t) {
                // print kalau error
                t.printStackTrace();
            }
        });
        return false;
    }
    // TODO : Alert dialog
    private void confirmDelete(final String id) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Hapus tempat ini ?");
        alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // delete place
                deletePlace(id);
            }
        });
        alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // 
            }
        });
        // Tampilkan Alert
        alert.show();
    }
    // TODO : Method delete buat kirim request ke server
    private void deletePlace(String id) {
        ApiServices api = InitLibrary.getInstance();
        Call<ResponseDelete> call = api.request_delete(id);
        call.enqueue(new Callback<ResponseDelete>() {
            @Override
            public void onResponse(Call<ResponseDelete> call, Response<ResponseDelete> response) {
                if (response.isSuccessful()) {
                    boolean isSuccess = response.body().isIsSuccess();
                    String msg = response.body().getMsg();
                    if (isSuccess) {
                        // refresh activity
                        Toast.makeText(PlacesActivity.this, msg, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PlacesActivity.this, PlacesActivity.class);
                        intent.putExtra("CAT_ID", cat_id);
                        intent.putExtra("CAT_NAME", cat_name);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(PlacesActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDelete> call, Throwable t) {
                // print error
                t.printStackTrace();
            }
        });
    }
}
