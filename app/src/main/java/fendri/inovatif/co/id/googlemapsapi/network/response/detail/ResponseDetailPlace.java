package fendri.inovatif.co.id.googlemapsapi.network.response.detail;

import com.google.gson.annotations.SerializedName;

public class ResponseDetailPlace{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private Data data;

	@SerializedName("isSuccess")
	private boolean isSuccess;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setIsSuccess(boolean isSuccess){
		this.isSuccess = isSuccess;
	}

	public boolean isIsSuccess(){
		return isSuccess;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDetailPlace{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",isSuccess = '" + isSuccess + '\'' + 
			"}";
		}
}