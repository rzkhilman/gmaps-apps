package fendri.inovatif.co.id.googlemapsapi.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fendri.inovatif.co.id.googlemapsapi.MainActivity;
import fendri.inovatif.co.id.googlemapsapi.R;
import fendri.inovatif.co.id.googlemapsapi.adapater.AdapaterListCategori;
import fendri.inovatif.co.id.googlemapsapi.adapater.AdapaterSearchList;
import fendri.inovatif.co.id.googlemapsapi.network.ApiServices;
import fendri.inovatif.co.id.googlemapsapi.network.InitLibrary;
import fendri.inovatif.co.id.googlemapsapi.network.response.search.DataItem;
import fendri.inovatif.co.id.googlemapsapi.network.response.search.ResponseSearch;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearcActivity extends AppCompatActivity {

    @BindView(R.id.rvSearchList)
    RecyclerView rvSearchList;
    @BindView(R.id.etKeyword)
    EditText etKeyword;
    @BindView(R.id.btnClear)
    ImageButton btnClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searc);
        ButterKnife.bind(this);
        // layout manager untuk recycler view
        rvSearchList.setLayoutManager(new LinearLayoutManager(this));
        // Ketika activity di jalankan tinggal kita eksekusi doSearch()
        doSearch(""); // bawa keyword kosong
        // Listenter event onClick pada btnSearh
        // eh kita blm kasih editext dan buttonya Id
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mengosongkan text jika toombol cari
                etKeyword.setText("");
            }
        });
        // TextWatcher ini gunannya untuk mendeteksi prubahan nilai di Edittext
        etKeyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String keyword = s.toString();
                doSearch(keyword);
                if(s.length() <= 0 ){
                    btnClear.setVisibility(View.GONE);
                } else {
                    btnClear.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void doSearch(String keyword) {
        // Isntance retrofit
        ApiServices api = InitLibrary.getInstance();
        // Siapkan request
        Call<ResponseSearch> call = api.request_search(keyword);
        // kirim request
        call.enqueue(new Callback<ResponseSearch>() {
            @Override
            public void onResponse(Call<ResponseSearch> call, Response<ResponseSearch> response) {
                if (response.isSuccessful()){
                    // tampung data dalam variable
                    boolean isSuccess = response.body().isIsSuccess();
                    List<DataItem> data_place = response.body().getData();
                    // tampilkan di Log dulu
                    Log.d("hasil cari", response.body().toString());
                    if (isSuccess){
                        // Siapkan Adapter
                        AdapaterSearchList adapter = new AdapaterSearchList(SearcActivity.this, data_place);
                        // bawa context & data hasil pencarian
                        rvSearchList.setAdapter(adapter);
                    } else {
                        //Toast.makeText(SearcActivity.this, "Pencarian tidak ditemukan !", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(SearcActivity.this, "Terjadi kesalahan " + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseSearch> call, Throwable t) {
                // print ketika error
                t.printStackTrace();
            }
        });
    }
    // Jalankan method untk back
    public void goBack(View view) {
        finish();
    }
}
