package fendri.inovatif.co.id.googlemapsapi.network.response;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("id")
	private String id;

	@SerializedName("ket")
	private String ket;

	@SerializedName("title")
	private String title;

	@SerializedName("gambar")
	private String gambar;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setKet(String ket){
		this.ket = ket;
	}

	public String getKet(){
		return ket;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setGambar(String gambar){
		this.gambar = gambar;
	}

	public String getGambar(){
		return gambar;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"id = '" + id + '\'' + 
			",ket = '" + ket + '\'' + 
			",title = '" + title + '\'' + 
			",gambar = '" + gambar + '\'' + 
			"}";
		}
}