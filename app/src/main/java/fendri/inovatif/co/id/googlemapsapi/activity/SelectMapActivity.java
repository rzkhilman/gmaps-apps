package fendri.inovatif.co.id.googlemapsapi.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fendri.inovatif.co.id.googlemapsapi.MainActivity;
import fendri.inovatif.co.id.googlemapsapi.R;

public class SelectMapActivity extends FragmentActivity implements OnMapReadyCallback {


    @BindView(R.id.tvLatLng)
    TextView tvLatLng;
    @BindView(R.id.btnSet)
    Button btnSet;
    private GoogleMap mMap;
    // Kit buat variable Global untuk menampung Lat long
    double LATITUDE, LONGITUDE;

    // Deklarasi
    FusedLocationProviderClient mFusedLocation;
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_map);
        ButterKnife.bind(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Inisialisasi
        mFusedLocation = LocationServices.getFusedLocationProviderClient(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-2.9547949, 104.6929232);
        mMap.setMyLocationEnabled(true);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Palembag"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mMap.clear(); // clear marker
                // set variable
                LATITUDE = latLng.latitude;
                LONGITUDE = latLng.longitude;
                // Set marker
                mMap.addMarker(new MarkerOptions().position(latLng).title("Lokasi"));
                // set Lat long ke text view
                tvLatLng.setText("Latitude : " + latLng.latitude + " \nLongitude : " + latLng.longitude);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12F));
                // 
                Toast.makeText(SelectMapActivity.this, "Lokasi di set", Toast.LENGTH_SHORT).show();
            }
        });
        // Dapatkan lokasi terakhir
        //https://medium.com/@rizal_hilman/mendapatkan-lokasi-koordinat-saat-ini-di-android-994d3eb555c6

        mFusedLocation.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    // Do it all with location
                    Log.d("My Current location", "Lat : " + location.getLatitude() + " Long : " + location.getLongitude());
                    LATITUDE = location.getLatitude();
                    LONGITUDE = location.getLongitude();
                    // Set marker
                    mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title("Lokasi"));
                    // set Lat long ke text view
                    tvLatLng.setText("Latitude : " + location.getLatitude() + " \nLongitude : " + location.getLongitude());
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12F));
                    //
                    Toast.makeText(SelectMapActivity.this, "Lokasi di set", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @OnClick(R.id.btnSet)
    public void onViewClicked() {
        // ketika button ini di pilih kita akan finish() activity ini
        // jadi kembali ke activity sblumnya
        Intent intent = new Intent();
        intent.putExtra("LAT", LATITUDE); // kita ingat KEY dan TIPE DATA nya
        intent.putExtra("LONG", LONGITUDE);
        // set Result agar bisa kita oper datanya ke Activity Add
        setResult(RESULT_OK, intent); // cara dapetinya pakai getIntent()
        // akhiri Activity ini
        finish();
    }
}
