/**
 * Automatically generated file. DO NOT MODIFY
 */
package fendri.inovatif.co.id.googlemapsapi;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "fendri.inovatif.co.id.googlemapsapi";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
