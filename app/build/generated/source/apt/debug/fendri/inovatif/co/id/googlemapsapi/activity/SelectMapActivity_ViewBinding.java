// Generated code from Butter Knife. Do not modify!
package fendri.inovatif.co.id.googlemapsapi.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import fendri.inovatif.co.id.googlemapsapi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectMapActivity_ViewBinding implements Unbinder {
  private SelectMapActivity target;

  private View view2131230769;

  @UiThread
  public SelectMapActivity_ViewBinding(SelectMapActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SelectMapActivity_ViewBinding(final SelectMapActivity target, View source) {
    this.target = target;

    View view;
    target.tvLatLng = Utils.findRequiredViewAsType(source, R.id.tvLatLng, "field 'tvLatLng'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnSet, "field 'btnSet' and method 'onViewClicked'");
    target.btnSet = Utils.castView(view, R.id.btnSet, "field 'btnSet'", Button.class);
    view2131230769 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectMapActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvLatLng = null;
    target.btnSet = null;

    view2131230769.setOnClickListener(null);
    view2131230769 = null;
  }
}
