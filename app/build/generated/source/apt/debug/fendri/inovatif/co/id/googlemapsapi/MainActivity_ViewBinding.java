// Generated code from Butter Knife. Do not modify!
package fendri.inovatif.co.id.googlemapsapi;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131230813;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.rvCategoryList = Utils.findRequiredViewAsType(source, R.id.rvCategoryList, "field 'rvCategoryList'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.fabTambah, "field 'fabTambah' and method 'onFabClicked'");
    target.fabTambah = Utils.castView(view, R.id.fabTambah, "field 'fabTambah'", FloatingActionButton.class);
    view2131230813 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onFabClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvCategoryList = null;
    target.fabTambah = null;

    view2131230813.setOnClickListener(null);
    view2131230813 = null;
  }
}
