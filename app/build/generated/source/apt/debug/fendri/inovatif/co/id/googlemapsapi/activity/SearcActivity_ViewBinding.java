// Generated code from Butter Knife. Do not modify!
package fendri.inovatif.co.id.googlemapsapi.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import fendri.inovatif.co.id.googlemapsapi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearcActivity_ViewBinding implements Unbinder {
  private SearcActivity target;

  @UiThread
  public SearcActivity_ViewBinding(SearcActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SearcActivity_ViewBinding(SearcActivity target, View source) {
    this.target = target;

    target.rvSearchList = Utils.findRequiredViewAsType(source, R.id.rvSearchList, "field 'rvSearchList'", RecyclerView.class);
    target.etKeyword = Utils.findRequiredViewAsType(source, R.id.etKeyword, "field 'etKeyword'", EditText.class);
    target.btnClear = Utils.findRequiredViewAsType(source, R.id.btnClear, "field 'btnClear'", ImageButton.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SearcActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvSearchList = null;
    target.etKeyword = null;
    target.btnClear = null;
  }
}
