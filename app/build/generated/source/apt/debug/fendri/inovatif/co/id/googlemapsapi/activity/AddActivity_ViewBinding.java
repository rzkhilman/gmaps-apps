// Generated code from Butter Knife. Do not modify!
package fendri.inovatif.co.id.googlemapsapi.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import fendri.inovatif.co.id.googlemapsapi.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddActivity_ViewBinding implements Unbinder {
  private AddActivity target;

  private View view2131230766;

  private View view2131230768;

  private View view2131230767;

  @UiThread
  public AddActivity_ViewBinding(AddActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddActivity_ViewBinding(final AddActivity target, View source) {
    this.target = target;

    View view;
    target.etPlaceName = Utils.findRequiredViewAsType(source, R.id.etPlaceName, "field 'etPlaceName'", EditText.class);
    target.spPlaceCategory = Utils.findRequiredViewAsType(source, R.id.spPlaceCategory, "field 'spPlaceCategory'", Spinner.class);
    target.etLatitude = Utils.findRequiredViewAsType(source, R.id.etLatitude, "field 'etLatitude'", EditText.class);
    target.etLongitude = Utils.findRequiredViewAsType(source, R.id.etLongitude, "field 'etLongitude'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnOpenMap, "field 'btnOpenMap' and method 'onBtnOpenMapClicked'");
    target.btnOpenMap = Utils.castView(view, R.id.btnOpenMap, "field 'btnOpenMap'", ImageButton.class);
    view2131230766 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnOpenMapClicked();
      }
    });
    target.etStreetAddress = Utils.findRequiredViewAsType(source, R.id.etStreetAddress, "field 'etStreetAddress'", EditText.class);
    target.etPhoneNumber = Utils.findRequiredViewAsType(source, R.id.etPhoneNumber, "field 'etPhoneNumber'", EditText.class);
    view = Utils.findRequiredView(source, R.id.btnSelectPhoto, "field 'btnSelectPhoto' and method 'onBtnSelectPhotoClicked'");
    target.btnSelectPhoto = Utils.castView(view, R.id.btnSelectPhoto, "field 'btnSelectPhoto'", Button.class);
    view2131230768 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnSelectPhotoClicked();
      }
    });
    target.tvFilename = Utils.findRequiredViewAsType(source, R.id.tvFilename, "field 'tvFilename'", TextView.class);
    view = Utils.findRequiredView(source, R.id.btnSavePlace, "field 'btnSavePlace' and method 'onBtnSavePlaceClicked'");
    target.btnSavePlace = Utils.castView(view, R.id.btnSavePlace, "field 'btnSavePlace'", Button.class);
    view2131230767 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBtnSavePlaceClicked();
      }
    });
    target.spPlaceType = Utils.findRequiredViewAsType(source, R.id.spPlaceType, "field 'spPlaceType'", Spinner.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.etPlaceName = null;
    target.spPlaceCategory = null;
    target.etLatitude = null;
    target.etLongitude = null;
    target.btnOpenMap = null;
    target.etStreetAddress = null;
    target.etPhoneNumber = null;
    target.btnSelectPhoto = null;
    target.tvFilename = null;
    target.btnSavePlace = null;
    target.spPlaceType = null;

    view2131230766.setOnClickListener(null);
    view2131230766 = null;
    view2131230768.setOnClickListener(null);
    view2131230768 = null;
    view2131230767.setOnClickListener(null);
    view2131230767 = null;
  }
}
